#ifndef ARBITRATOR_H
#define ARBITRATOR_H

#include "betteristrategy.h"


class Arbitrator
{
private:
    bool specialBoard;
    std::vector<int> board;
    int games;
    BetterIStrategy* first;
    BetterIStrategy* second;
    bool play();
public:
    Arbitrator(BetterIStrategy* first, BetterIStrategy* second, int games);
    Arbitrator(BetterIStrategy* first, BetterIStrategy* second, int games, std::vector<int> board);
    void setBoard(std::vector<int> board);
    int firstWins();
};

#endif // ARBITRATOR_H
