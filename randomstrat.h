#ifndef RANDOMSTRAT_H
#define RANDOMSTRAT_H

#include "betteristrategy.h"
#include "movesgenerator.h"

class RandomStrat : public BetterIStrategy
{
private:
    MovesGenerator generator;
    bool isWhite;
protected:
    TMove moveFromPos(const std::vector<TMove> &possibleMoves);
public:
    RandomStrat(bool isWhite);
    //~RandomStrat();

    TMove move (const std::pair<int, int> &dice);
    void update(const TMove &moves);
    void reset();
    void changeSide();
    void setBoard(const std::vector<int>& board);
};

#endif // RANDOMSTRAT_H
