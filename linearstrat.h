#ifndef LINEARSTRAT_H
#define LINEARSTRAT_H

#include "betteristrategy.h"
#include "movesgenerator.h"

class LinearStrat : public BetterIStrategy
{
protected:
    bool isWhite;
    std::vector<int> board;
    MovesGenerator generator;

    std::vector<double> coeffsMe, coeffsEnemy, coeffsWalls, coeffsWalls2, coeffsEnemyWalls, coeffsEnemyWalls2;
    double coeffLast, coeffFirst, coeffNumMe, coeffNumEnemy, coeffAreaMe, coeffAreaEnemy;

    TMove moveFromPos(const std::vector<TMove>& possibleMoves);
public:
    double possibility(const std::vector<int>& pos);
    TMove move(const std::pair<int, int>& dice);
    void update(const TMove& moves);
    void reset();
    void changeSide();
    void setBoard(const std::vector<int>& board);
};

#endif // LINEARSTRAT_H
