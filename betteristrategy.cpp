#include "betteristrategy.h"

TMove BetterIStrategy::GetMove(const std::vector<int> &board, const std::vector<TMove> &possibleMoves) {
    bool isWhite;
    TMove move;
    if (!possibleMoves.size())
        return {};
    isWhite = possibleMoves[0][0].first > 0;
    setBoard(board);
    std::vector<TMove> copy = possibleMoves;
    if (!isWhite) {
        for (auto &a : copy)
            for (auto &b : a) {
                b.first = (b.first + 12) % 24;
                if (b.second > -1)
                    b.second = (b.second + 12) % 24;
            }
    }
    move = moveFromPos(possibleMoves);
    if (!isWhite)
        for (std::pair<int, int>& el : move) {
            el.first = (el.first + 12) % 24;
            if (el.second > -1)
                el.second = (el.second + 12) % 24;
        }
    return move;
}
