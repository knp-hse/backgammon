#include "neuronstrat.h"
#include <fstream>
#include <iostream>
#include <limits>
#include <algorithm>
#include <math.h>
#include "arbitrator.h"
#include "randomstrat.h"

NeuronStrat::NeuronStrat(bool isWhite, size_t layers, size_t params, size_t layersize,
                         ParamsCalculator calc, std::string filename):
    isWhite(isWhite), layers(layers), params(params), board(24, 0),
    layersize(layersize), calc(calc), filename(filename), coeffs(layers) {
    board[0] = 15;
    board[12] = -15;
    useIntercepts = 0;
    coeffs[0] = std::vector<std::vector<double>>(params, std::vector<double>(layersize, 0));
    for (size_t i = 1; i < layers - 1; i++) {
        coeffs[i] = std::vector<std::vector<double>>(layersize, std::vector<double>(layersize, 0));
    }
    coeffs[layers-1] = std::vector<std::vector<double>>(layersize, std::vector<double>(1, 0));

    std::ifstream file(filename);
    if (file.is_open()) {
        for (size_t i = 0; i < params; i++)
            for (size_t j = 0; j < layersize; j++)
                file >> coeffs[0][i][j];
        for (size_t k = 1; k < layers - 1; k++)
            for (size_t i = 0; i < layersize; i++)
                for (size_t j = 0; j < layersize; j++)
                    file >> coeffs[k][i][j];
        for (size_t i = 0; i < layersize; i++)
            file >> coeffs[layers-1][i][0];
    } else
        std::cout << "Unable to open file, setting all coeffs to 0\n";
}

void NeuronStrat::setIntercepts(std::string filename) {
    useIntercepts = 1;
    std::ifstream file(filename);
    intercepts = std::vector<std::vector<double>>(layers);
    if (file.is_open()) {
        for (int i = 0; i < layers-1; i++) {
            intercepts[i] = std::vector<double>(layersize);
            for (int j = 0; j < layersize; j++)
                file >> intercepts[i][j];
        }
        double tmp;
        file >> tmp;
        intercepts[layers-1] = {tmp};
    } else
        std::cout << "Unable to open intercepts file\n";
}

void NeuronStrat::writeCoeffs() {
    std::ofstream file(filename);
    if (file.is_open()) {
        for (size_t i = 0; i < params; i++)
            for (size_t j = 0; j < layersize; j++)
                file << coeffs[0][i][j];
        for (size_t k = 1; k < layers; k++)
            for (size_t i = 0; i < layersize; i++)
                for (size_t j = 0; j < layersize; j++)
                    file << coeffs[k][i][j];
        for (size_t i = 0; i < layersize; i++)
            file << coeffs[layers-1][i][0];
    } else
        std::cout << "Unable to write coeffs";
}

double NeuronStrat::sigmoid(double x) {
    return 1.0/(1.0 + exp(-1 * x));
}

double NeuronStrat::possibility(const std::vector<int>& board) {
    std::vector<double> layer(params), nextlayer;
    calc(board, layer);
    for (size_t i = 0; i < layers; i++) {
        nextlayer = std::vector<double>(coeffs[i][0].size(), 0);
        for (size_t j = 0; j < nextlayer.size(); j++) {
            for (size_t k = 0; k < layer.size(); k++)
                nextlayer[j] += layer[k] * coeffs[i][k][j];
            if (useIntercepts)
                nextlayer[j] += intercepts[i][j];
            nextlayer[j] = sigmoid(nextlayer[j]);
        }
        layer = nextlayer;
    }
    return layer[0];
}

std::vector<int> NeuronStrat::genRandomPos(BetterIStrategy* first, BetterIStrategy* second) {
    int turns = 2 + rand() % 48;
    //std::cout << turns << std::endl;
    TMove tmp;
    std::vector<int> tmpBoard(24, 0);
    bool flag;
    tmpBoard[0] = 15;
    tmpBoard[12] = -15;
    first->reset();
    second->reset();
    for(int i = 0; i < turns; i++) {
        //white's turn
        flag = 1;
        tmp = first->move(std::make_pair(rand() % 6 + 1, rand() % 6 + 1));

        for (auto elem : tmp) {
            tmpBoard[elem.first]--;
            if (elem.second > -1)
                tmpBoard[elem.second]++;
        }
        for (int i : tmpBoard)
            if (i > 0)
                flag = 0;
        if (flag)
            return tmpBoard;
        second->update(tmp);

        //black's turn
        flag = 1;
        tmp = second->move(std::make_pair(rand() % 6 + 1, rand() % 6 + 1));

        for (auto elem : tmp) {
            tmpBoard[elem.first]++;
            if (elem.second > -1)
                tmpBoard[elem.second]--;
        }
        for (int i : tmpBoard)
            if (i < 0)
                flag = 0;
        if (flag)
            return tmpBoard;
        first->update(tmp);
    }
    return tmpBoard;
}

void NeuronStrat::generateLearningData(BetterIStrategy *first, BetterIStrategy *second, size_t games, ParamsCalculator calc,
                                       size_t params, std::string filename) {
    std::vector<double> res = std::vector<double>(params);
    std::ofstream out(filename);
    if (!out.is_open()) {
        std::cout << "Unable to write learning data";
        return;
    }
    first->reset();
    second->reset();
    std::vector<int> brd(24);
    Arbitrator arb(first, second, 100);
    RandomStrat pl_first(1), pl_second(0);
    for (size_t i = 0; i < games; i++) {
        std::cout << i << std::endl;
        brd = genRandomPos(&pl_first, &pl_second);
        arb.setBoard(brd);
        calc(brd, res);
        for (double param : res)
            out << param << ' ';
        out << ((double)arb.firstWins()) / 100 << '\n';
    }
}

TMove NeuronStrat::moveFromPos(const std::vector<TMove> &possibleMoves) {
    TMove move;
    double best = std::numeric_limits<double>::lowest(), tmp;

    for (TMove m : possibleMoves) {
        for (std::pair<int, int> el : m) {
            board[el.first]--;
            if (el.second > -1)
                board[el.second]++;
        }
        tmp = possibility(board);
        if (tmp > best) {
            best = tmp;
            move = m;
        }
        for (std::pair<int, int> el : m) {
            board[el.first]++;
            if (el.second > -1)
                board[el.second]--;
        }
    }
    return move;
}

TMove NeuronStrat::move(const std::pair<int, int> &dice) {
    TMove move;
    std::vector<TMove> ans;
    generator.getMoves(dice, ans);
    if (!ans.size())
        return {};
    move = moveFromPos(ans);

    generator.update(move, 1);

    for (std::pair<int, int>& el : move) {
        board[el.first]--;
        if (el.second > -1)
            board[el.second]++;
        if (!isWhite) {
            el.first = (el.first + 12) % 24;
            if (el.second > -1)
                el.second = (el.second + 12) % 24;
        }
    }

    std::reverse(move.begin(), move.end());

    return move;
}

void NeuronStrat::update(const TMove& moves) {
    TMove ans = moves;
    if (!isWhite)
        for (size_t i = 0; i < ans.size(); i++) {
            ans[i].first = (ans[i].first + 12) % 24;
            if (ans[i].second > -1)
                ans[i].second = (ans[i].second + 12) % 24;
        }
    generator.update(ans, 0);
    for (std::pair<int, int> move : ans) {
        board[move.first]++;
        if (move.second > -1)
            board[move.second]--;
    }
}

void NeuronStrat::setBoard(const std::vector<int> &board) {
    if (!isWhite)
        for (int i = 0; i < 24; i++)
            this->board[i] = -1 * board[(i + 12) % 24];
    else
        this->board = board;
    generator = MovesGenerator(this->board);
}

void NeuronStrat::changeSide() {
    isWhite = !isWhite;
}

void NeuronStrat::reset() {
    generator.reset();
    board = std::vector<int>(24, 0);
    board[0] = 15;
    board[12] = -15;
}
