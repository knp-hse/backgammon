#ifndef ISTRATEGY_H
#define ISTRATEGY_H
#include <vector>

typedef std::vector<std::pair<int, int>> TMove;

class IStrategy {
public:
    virtual ~IStrategy(){}
    virtual TMove GetMove(const std::vector<int>& board, const std::vector<TMove>& possibleMoves) = 0;
};

#endif // ISTRATEGY_H
