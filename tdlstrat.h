#ifndef TDLSTRAT_H
#define TDLSTRAT_H

#include "linearstrat.h"


class TDLstrat : public LinearStrat
{
private:
    void reloadCoeffs();
    void writeCoeffs();
public:
    TDLstrat(bool isWhite);

    void learn(double lambda, int games);
};

#endif // TDLSTRAT_H
