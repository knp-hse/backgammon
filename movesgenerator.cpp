#include "movesgenerator.h"
#include <algorithm>
#include <iostream>

MovesGenerator::MovesGenerator() {
    board = std::vector<int>(24, 0);
    board[0] = 15;
    board[12] = -15;
    first = last = 0;
}

MovesGenerator::MovesGenerator(const std::vector<int> &board) {
    this->board = board;
    for (int i = 0; i < 24; i++)
        if (board[i] > 0) {
            first = i;
            break;
        }
    for (int i = 23; i > -1; i--)
        if (board[i] > 0) {
            last = i;
            break;
        }
}

bool MovesGenerator::isValidMove(const std::pair<int, int> &move) {
    int wallLength = 1;
    if (move.second < 24)  {
        if (board[move.second] < 0)
            return false;
        else  {
            board[move.first]--;

            if (move.second != 11)
                for (int i = (move.second + 13) % 24; i < 24 && board[(i + 12) % 24] > 0; i++)
                    wallLength++;
            for (int i = (move.second + 11) % 24; i > -1 && board[(i + 12) % 24] > 0; i--)
                wallLength++;

            if (wallLength > 5) {
                for (int i = 23; i > (move.second + 12) % 24; i--)
                    if (board[(i + 12) % 24] < 0) {
                        board[move.first]++;
                        return true;
                    }
                board[move.first]++;
                return false;
            }
            board[move.first]++;
            return true;
        }
    }

    //move.second >= 24
    for (int i = 0; i < 18; i++)
        if (board[i] > 0)
            return false;

    if (move.second == 24)
        return true;
    for (int i = 18; i < move.first; i++)
        if (board[i] > 0) {
            return false;
        }
    return true;
}

void MovesGenerator::generateMoves (std::vector<int>::iterator diceBegin, std::vector<int>::iterator diceEnd,
                                    bool movedFromHead, int from, int to, std::vector<TMove>& ans) {
    TMove foo(1);
    foo.reserve(4);
    std::vector<TMove> bar;
    int newfrom, newto;
    for (int pos = std::max(movedFromHead ? 1 : 0, from); pos <= to && pos < 24; pos++) {
        if (board[pos] > 0 && isValidMove(std::make_pair(pos, pos + *diceBegin))) {
            newfrom = from;
            newto = to;

            board[pos]--;

            if ((pos + *diceBegin) < 24) {
                board[pos + *diceBegin]++;
                if (pos + *diceBegin > to)
                    newto = pos + *diceBegin;
            }

            if (pos == from && board[pos] == 0) {
                while (newfrom < 24 && board[newfrom] <= 0)
                    newfrom++;
            }

            if (diceBegin + 1 == diceEnd) {
                if ((pos + *diceBegin) < 24)
                    foo[0] = std::make_pair(pos, pos + *diceBegin);
                else
                    foo[0] = std::make_pair(pos, -1);
                ans.push_back(foo);
            } else {
                generateMoves(diceBegin + 1, diceEnd, movedFromHead || pos == 0, newfrom, newto, bar);
                for (TMove elem : bar) {
                    if ((pos + *diceBegin) < 24)
                        elem.push_back(std::make_pair(pos, pos + *diceBegin));
                    else
                        elem.push_back(std::make_pair(pos, -1));
                    ans.push_back(elem);
                }
                bar.clear();
            }
            board[pos]++;
            if ((pos + *diceBegin) < 24)
                board[pos + *diceBegin]--;
        }
    }
}

void MovesGenerator::getMoves (const std::pair<int, int>& dice, std::vector<TMove>& ans) {
    std::vector<int> myDice;
    TMove foo;

    //first turn exceptions
    if (board[0] == 15 && dice.first == dice.second) {
        if (dice.first == 3) {
            foo.push_back(std::make_pair(0, 3));
            foo.push_back(std::make_pair(3, 6));
            foo.push_back(std::make_pair(6, 9));
            foo.push_back(std::make_pair(0, 3));
            ans.push_back(foo);
            foo.clear();
            foo.push_back(std::make_pair(0, 3));
            foo.push_back(std::make_pair(3, 6));
            foo.push_back(std::make_pair(0, 3));
            foo.push_back(std::make_pair(3, 6));
            ans.push_back(foo);
            return;
        }
        if (dice.first == 4 && board[8] == 0) {
            foo.push_back(std::make_pair(0, 4));
            foo.push_back(std::make_pair(4, 8));
            foo.push_back(std::make_pair(0, 4));
            foo.push_back(std::make_pair(4, 8));
            ans.push_back(foo);
            return;
        }
        if (dice.first == 6) {
            foo.push_back(std::make_pair(0, 6));
            foo.push_back(std::make_pair(0, 6));
            ans.push_back(foo);
            return;
        }

    }

    if (dice.first != dice.second) {
        myDice = {dice.first, dice.second};
        generateMoves(myDice.begin(), myDice.end(), 0, first, last, ans);
        myDice = {dice.second, dice.first};
        generateMoves(myDice.begin(), myDice.end(), 0, first, last, ans);
        if (ans.empty()) {
            myDice = {std::max(dice.first, dice.second) };
            generateMoves(myDice.begin(), myDice.end(), 0, first, last, ans);
            if (ans.empty()) {
                myDice = {std::min(dice.first, dice.second) };
                generateMoves(myDice.begin(), myDice.end(), 0, first, last, ans);
            }
        }
    } else {
        myDice = {dice.first, dice.first, dice.first, dice.first};
        for (int i = 0; i < 4 && ans.empty(); i++) {
            generateMoves(myDice.begin(), myDice.end() - i, 0, first, last, ans);
        }
    }
}

void MovesGenerator::update(const TMove &moves, bool isMyTurn) {
    //std::cout << first << ' ' << last << '\n';
    for (auto elem : moves) {
        if (isMyTurn) {
            board[elem.first]--;
            if (elem.second > -1) {
                board[elem.second]++;
                if (elem.second > last) {
                    last = elem.second;
                }
            }
            if (elem.first == first && board[first] == 0) {
                while (first < 24 && board[first] <= 0)
                    first++;
            }
        } else {
            board[elem.first]++;
            if (elem.second > -1)
                board[elem.second]--;
        }
    }
}

void MovesGenerator::reset() {
    board = std::vector<int>(24, 0);
    board[0] = 15;
    board[12] = -15;
    first = last = 0;
}
