#include <iostream>
#include <time.h>
#include <cstring>
#include "arbitrator.h"
#include "randomstrat.h"
#include "randomlearningstrat.h"
#include "tdlstrat.h"
#include "neuronstrat.h"

void printBoard(std::vector<int> board) {
    for (int i = 0; i < 12; i++)
        std::cout << "  " << 23 - i <<" ";
    std::cout << '\n';
    for (int i = 0; i < 12; i++) {
        if (board[23-i] < 0)
            std::cout << "#####";
        else if (board[23-i] > 0)
            std::cout << "_____";
        else
            std::cout << "     ";
    }
    std::cout << '\n';
    for (int i = 0; i < 12; i++) {
        if (board[23-i] < 0)
            std::cout << "#x" << (board[23-i] < -9 ? "" : "0") << -1 * board[23-i] << "#";
        else if (board[23-i] > 0)
            std::cout << "_x" << (board[23-i] > 9 ? "" : "0") << board[23-i] << "_";
        else
            std::cout << "     ";
    }
    std::cout << '\n';
    for (int i = 0; i < 12; i++) {
        if (board[23-i] < 0)
            std::cout << "#####";
        else if (board[23-i] > 0)
            std::cout << "_____";
        else
            std::cout << "     ";
    }
    std::cout << "\n\n\n\n\n\n";

    for (int i = 0; i < 12; i++) {
        if (board[i] < 0)
            std::cout << "#####";
        else if (board[i] > 0)
            std::cout << "_____";
        else
            std::cout << "     ";
    }
    std::cout << '\n';
    for (int i = 0; i < 12; i++) {
        if (board[i] < 0)
            std::cout << "#x" << (board[i] < -9 ? "" : "0") << -1 * board[i] << "#";
        else if (board[i] > 0)
            std::cout << "_x" << (board[i] > 9 ? "" : "0") << board[i] << "_";
        else
            std::cout << "     ";
    }
    std::cout << '\n';
    for (int i = 0; i < 12; i++) {
        if (board[i] < 0)
            std::cout << "#####";
        else if (board[i] > 0)
            std::cout << "_____";
        else
            std::cout << "     ";
    }
    std::cout << '\n';
    for (int i = 0; i < 12; i++)
        std::cout << "  " << i << (i > 9 ? "" : " ") <<" ";
    std::cout << '\n';
}

bool playvsAI(BetterIStrategy* strat, bool isWhite) {
    if (!isWhite)
        strat->changeSide();
    int foo, from, to;
    std::pair<int, int> turn;
    TMove tmp;
    bool flag;
    std::vector<int> tmpBoard(24, 0);
    tmpBoard[0] = 15;
    tmpBoard[12] = -15;
    for(;;) {
        //white's turn
        printBoard(tmpBoard);
        turn = std::make_pair(rand() % 6 + 1, rand() % 6 + 1);
        flag = 1;
        std::cout << "White rolls for: " << turn.first << ' ' << turn.second << '\n';

        if (!isWhite) {
            std::cout << "White's turn:\n";
            tmp = strat->move(turn);
            for (auto elem : tmp) {
                std::cout << elem.first << ' ' << elem.second << '\n';
                tmpBoard[elem.first]--;
                if (elem.second > -1)
                    tmpBoard[elem.second]++;
            }
            for (int i : tmpBoard)
                if (i > 0)
                    flag = 0;
            if (flag)
                return 1;
        } else {
            tmp.clear();
            std::cout << "Enter number of moves in your turn:\n";
            std::cin >> foo;
            std::cout << "Enter your turn:\n";
            for (int i = 0; i < foo; i++) {
                std::cin >> from >> to;
                tmp.push_back(std::make_pair(from, to));
                tmpBoard[from]--;
                if (to > -1)
                    tmpBoard[to]++;
            }
            for (int i : tmpBoard)
                if (i > 0)
                    flag = 0;
            if (flag)
                return 1;
            strat->update(tmp);
        }

        //black's turn
        printBoard(tmpBoard);
        turn = std::make_pair(rand() % 6 + 1, rand() % 6 + 1);
        flag = 1;
        std::cout << "Black rolls for: " << turn.first << ' ' << turn.second << '\n';

        if (isWhite) {
            std::cout << "Black's turn:\n";
            tmp = strat->move(turn);
            for (auto elem : tmp) {
                std::cout << elem.first << ' ' << elem.second << '\n';
                tmpBoard[elem.first]++;
                if (elem.second > -1)
                    tmpBoard[elem.second]--;
            }
            for (int i : tmpBoard)
                if (i < 0)
                    flag = 0;
            if (flag)
                return 0;
        } else {
            tmp.clear();
            std::cout << "Enter number of moves in your turn:\n";
            std::cin >> foo;
            std::cout << "Enter your turn:\n";
            for (int i = 0; i < foo; i++) {
                std::cin >> from >> to;
                tmp.push_back(std::make_pair(from, to));
                tmpBoard[from]++;
                if (to > -1)
                    tmpBoard[to]--;
            }
            for (int i : tmpBoard)
                if (i < 0)
                    flag = 0;
            if (flag)
                return 0;
            strat->update(tmp);
        }
    }
}

void defaultParams(const std::vector<int>& tmp, std::vector<double>& params) {
    std::vector<int> valueMe(24), valueEnemy(24), valueWalls(24), valueWalls2(24), valueEnemyWalls(24), valueEnemyWalls2(24);
    int valueFirst, valueLast, valueNumMe, valueNumEnemy, valueAreaMe, valueAreaEnemy;
    bool firstFlag = 0;

    valueLast = valueNumMe = valueNumEnemy = valueAreaMe =
            valueAreaEnemy = 0;
    valueFirst = 24;

    for (int i = 0; i < 24; i++) {
        valueEnemyWalls[i] = valueWalls[i] =
                valueEnemyWalls2[i] = valueWalls2[i] = 0;
        if (tmp[i] < 0) {
            valueAreaEnemy++;
            valueNumEnemy -= tmp[i];
            valueMe[i] = 0;
            valueEnemy[i] = -tmp[i];
            for (int j = 0; tmp[(i + j) % 24] < 0; j++)
                valueEnemyWalls[i]++;
            valueEnemyWalls2[i] = valueEnemyWalls[i] * valueEnemyWalls[i];
        } else if (tmp[i] > 0) {
            if (!firstFlag) {
                firstFlag = 1;
                valueFirst = i;
            }
            if (i > valueLast)
                valueLast = i;
            valueAreaMe++;
            valueNumMe += tmp[i];
            valueEnemy[i] = 0;
            valueMe[i] = tmp[i];
            for (int j = 0; tmp[(i + j) % 24] > 0; j++)
                valueWalls[i]++;
        } else {
            valueMe[i] = valueEnemy[i] = 0;
        }
    }

    if (!valueNumMe)
        valueFirst = valueLast = 24;

    int n = -1;
    for (int i = 0; i < 24; i++) {
        params[++n] = valueMe[i];
        params[++n] = valueEnemy[i];
        params[++n] = valueWalls[i];
        params[++n] = valueWalls2[i];
        params[++n] = valueEnemyWalls[i];
        params[++n] = valueEnemyWalls2[i];
    }
    params[++n] = valueFirst;
    params[++n] = valueLast;
    params[++n] = valueNumMe;
    params[++n] = valueNumEnemy;
    params[++n] = valueAreaMe;
    params[++n] = valueAreaEnemy;
}

void printHelp() {
    std::cout << "Usage:\n" <<
                 "-help: displays this message\n" <<
                 "-play: starts a game vs ai, allows to select strategy and side\n" <<
                 "-generate N FILENAME: generates dataset with N positions and writes it to FILENAME\n";
}

int main(int argc, char* argv[]) {
    if (argc == 1 || argc > 3 || !strcmp(argv[1],"-help")) {
        std::cout << argc;
        printHelp();
        return 0;
    }
    srand(time(0));

    if (!strcmp(argv[1],"-generate")) {
        if (argc < 4)
            printHelp();
        else
            RandomLearningStrat::generateLearningData(atoi(argv[2]), argv[3]);
        return 0;
    }

    if (strcmp(argv[1], "-play")) {
        printHelp();
        return 0;
    }

    std::string tmp;
    bool isWhite = 1, win;
    NeuronStrat neuro(0, 3, 150, 15, &defaultParams);
    neuro.setIntercepts("inter.txt");
    RandomLearningStrat rls(0);
    RandomStrat rand(0);
    TDLstrat tdl(0);

    std::cout << "Enter your color(black/white):\n";
    std::cin >> tmp;
    if (tmp == "black")
        isWhite = 0;
    std::cout << "Enter your opponent's strat (random, linear-set, linear-tdl, neuron):\n";
    std::cin >> tmp;
    if (tmp == "random")
        win = playvsAI(&rand, isWhite);
    else if (tmp == "linear-set")
        win = playvsAI(&rls, isWhite);
    else if (tmp == "linear-tdl")
        win = playvsAI(&tdl, isWhite);
    else
        win = playvsAI(&neuro, isWhite);

    if (win == isWhite)
        std::cout << "You won!";
    else
        std::cout << "AI won!";

    return 0;
}
