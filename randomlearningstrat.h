#ifndef RANDOMLEARNINGSTRAT_H
#define RANDOMLEARNINGSTRAT_H

#include <string>
#include "linearstrat.h"
#include "randomstrat.h"


class RandomLearningStrat : public LinearStrat
{
private:

    static std::vector<int> genRandomPos(RandomStrat &first, RandomStrat &second);
public:
    RandomLearningStrat(bool isWhite);

    static void generateLearningData(int games, std::string filename);
};

#endif // RANDOMLEARNINGSTRAT_H
