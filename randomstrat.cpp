#include "randomstrat.h"
#include <algorithm>
#include <stdlib.h>

RandomStrat::RandomStrat(bool isWhite) {
    //srand(time(0));
    this->isWhite = isWhite;
    generator = MovesGenerator();
}


TMove RandomStrat::move(const std::pair<int, int> &dice) {
    std::vector<TMove> moves;
    generator.getMoves(dice, moves);
    if (!moves.size())
        return {};
    TMove ans = moves[rand() % moves.size()];
    std::reverse(ans.begin(), ans.end());
    generator.update(ans, 1);

    if (!isWhite)
        for (int i = 0; i < ans.size(); i++) {
            ans[i].first = (ans[i].first + 12) % 24;
            if (ans[i].second > -1)
                ans[i].second = (ans[i].second + 12) % 24;
        }
    return ans;
}

void RandomStrat::update(const TMove &moves) {
    TMove ans = moves;
    if (!isWhite)
        for (int i = 0; i < ans.size(); i++) {
            ans[i].first = (ans[i].first + 12) % 24;
            if (ans[i].second > -1)
                ans[i].second = (ans[i].second + 12) % 24;
        }
    generator.update(ans, 0);
}

void RandomStrat::reset() {
    generator.reset();
}

void RandomStrat::changeSide() {
    isWhite = !isWhite;
}

void RandomStrat::setBoard(const std::vector<int> &board) {
    std::vector<int> myBoard = board;
    if (!isWhite)
        for (int i = 0; i < 24; i++)
            myBoard[i] = -1 * board[(i + 12) % 24];
    generator = MovesGenerator(myBoard);
}

TMove RandomStrat::moveFromPos(const std::vector<TMove> &possibleMoves) {
    return possibleMoves[rand() % possibleMoves.size()];
}
