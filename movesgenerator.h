#ifndef MOVESGENERATOR_H
#define MOVESGENERATOR_H

#include <vector>

typedef std::vector<std::pair<int, int>> TMove;

class MovesGenerator
{
protected:
    std::vector<int> board;
    int first, last;
    bool isValidMove (const std::pair<int, int>& move);
    void generateMoves(std::vector<int>::iterator diceBegin, std::vector<int>::iterator diceEnd,
                                      bool movedFromHead, int from, int to, std::vector<TMove> &ans);
public:
    MovesGenerator();
    MovesGenerator(const std::vector<int>& board);
    void getMoves(const std::pair<int, int>& dice, std::vector<TMove> &ans);
    void update(const TMove& moves, bool isMyTurn);
    void reset();
};

#endif // MOVESGENERATOR_H
