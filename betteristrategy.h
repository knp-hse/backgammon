#ifndef BETTERISTRATEGY_H
#define BETTERISTRATEGY_H

#include <vector>
#include "istrategy.h"

//typedef std::vector<std::pair<int, int>> TMove;

class BetterIStrategy : public IStrategy {
protected:
    virtual TMove moveFromPos(const std::vector<TMove>& possibleMoves) = 0;
public:
    virtual TMove move(const std::pair<int, int>& dice) = 0;
    virtual void update(const TMove& moves) = 0;
    virtual void reset() = 0;
    virtual void changeSide() = 0;
    virtual void setBoard(const std::vector<int>& board) = 0;
    TMove GetMove(const std::vector<int>& board, const std::vector<TMove>& possibleMoves);
};

#endif // BETTERISTRATEGY_H
