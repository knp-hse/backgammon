#include "linearstrat.h"
#include <limits>
#include <algorithm>

double LinearStrat::possibility(const std::vector<int>& pos) {
    double ans = 0;
    int area = 0, areaEnemy = 0, num = 0, numEnemy = 0, first = 24, last = 0, foo;
    bool firstFlag = 0;
    for (int i = 0; i < 24; i++) {
        foo = 0;
        if (pos[i] < 0) {
            areaEnemy++;
            numEnemy -= pos[i];
            ans -= pos[i] * coeffsEnemy[i];
            for (int j = 0; pos[(i + j) % 24] < 0; j++)
                foo++;
            ans += coeffsEnemyWalls[i] * foo;
            ans += coeffsEnemyWalls2[i] * foo *foo;
        } else if (pos[i] > 0) {
            if (!firstFlag) {
                firstFlag = 1;
                first = i;
            }
            if (i > last)
                last = i;
            area++;
            num += pos[i];
            ans += coeffsMe[i] * pos[i];
            for (int j = 0; pos[(i + j) % 24] > 0; j++)
                foo++;
            ans += coeffsWalls[i] * foo;
            ans += coeffsEnemyWalls2[i] * foo * foo;
        }
    }
    if (!firstFlag)
        last = first = 24;
    ans += coeffFirst * first + coeffLast * last + coeffNumMe * num +
            coeffNumEnemy * numEnemy + coeffAreaMe * area + coeffAreaEnemy * areaEnemy;
    return ans;
}

TMove LinearStrat::move(const std::pair<int, int>& dice) {
    TMove move;
    std::vector<TMove> ans;
    generator.getMoves(dice, ans);
    if (!ans.size())
        return {};
    move = moveFromPos(ans);

    generator.update(move, 1);

    for (std::pair<int, int>& el : move) {
        board[el.first]--;
        if (el.second > -1)
            board[el.second]++;
        if (!isWhite) {
            el.first = (el.first + 12) % 24;
            if (el.second > -1)
                el.second = (el.second + 12) % 24;
        }
    }

    std::reverse(move.begin(), move.end());

    return move;
}

void LinearStrat::update(const TMove& moves) {
    TMove ans = moves;
    if (!isWhite)
        for (int i = 0; i < ans.size(); i++) {
            ans[i].first = (ans[i].first + 12) % 24;
            if (ans[i].second > -1)
                ans[i].second = (ans[i].second + 12) % 24;
        }
    generator.update(ans, 0);
    for (std::pair<int, int> move : ans) {
        board[move.first]++;
        if (move.second > -1)
            board[move.second]--;
    }
}

void LinearStrat::setBoard(const std::vector<int> &board) {
    if (!isWhite)
        for (int i = 0; i < 24; i++)
            this->board[i] = -1 * board[(i + 12) % 24];
    else
        this->board = board;
    generator = MovesGenerator(this->board);
}

void LinearStrat::changeSide() {
    isWhite = !isWhite;
}

void LinearStrat::reset() {
    generator.reset();
    board = std::vector<int>(24, 0);
    board[0] = 15;
    board[12] = -15;
}

TMove LinearStrat::moveFromPos(const std::vector<TMove> &possibleMoves) {
    TMove move;
    double best = std::numeric_limits<double>::lowest(), tmp;

    for (TMove m : possibleMoves) {
        for (std::pair<int, int> el : m) {
            board[el.first]--;
            if (el.second > -1)
                board[el.second]++;
        }
        tmp = possibility(board);
        if (tmp > best) {
            best = tmp;
            move = m;
        }
        for (std::pair<int, int> el : m) {
            board[el.first]++;
            if (el.second > -1)
                board[el.second]--;
        }
    }
    return move;
}
