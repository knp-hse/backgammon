#include "randomlearningstrat.h"
#include <fstream>
#include <iostream>
#include "randomstrat.h"
#include "arbitrator.h"

RandomLearningStrat::RandomLearningStrat(bool isWhite) {
    this->isWhite = isWhite;
    board = std::vector<int>(24, 0);
    board[0] = 15;
    board[12] = -15;
    generator = MovesGenerator();

    coeffsMe = coeffsEnemy = coeffsWalls  = coeffsWalls2 =
            coeffsEnemyWalls = coeffsEnemyWalls2 = std::vector<double>(24);
    std::ifstream input("randomcoeffs.txt");
    if (input.is_open()) {
        for (int i = 0; i < 24; i++)
            input >> coeffsMe[i] >> coeffsEnemy[i] >> coeffsWalls[i] >> coeffsWalls2[i]
                    >> coeffsEnemyWalls[i] >> coeffsEnemyWalls2[i];
        input >> coeffFirst >> coeffLast >> coeffNumMe >> coeffNumEnemy >> coeffAreaMe
                >> coeffAreaEnemy;
    } else
        std::cout << "Unable to open coeffs file\n";
    input.close();
}

std::vector<int> RandomLearningStrat::genRandomPos(RandomStrat& first, RandomStrat& second) {
    int turns = 2 + rand() % 48;
    //std::cout << turns << std::endl;
    TMove tmp;
    std::vector<int> tmpBoard(24, 0);
    bool flag;
    tmpBoard[0] = 15;
    tmpBoard[12] = -15;
    first.reset();
    second.reset();
    for(int i = 0; i < turns; i++) {
        //white's turn
        flag = 1;
        tmp = first.move(std::make_pair(rand() % 6 + 1, rand() % 6 + 1));

        for (auto elem : tmp) {
            tmpBoard[elem.first]--;
            if (elem.second > -1)
                tmpBoard[elem.second]++;
        }
        for (int i : tmpBoard)
            if (i > 0)
                flag = 0;
        if (flag)
            return tmpBoard;
        second.update(tmp);

        //black's turn
        flag = 1;
        tmp = second.move(std::make_pair(rand() % 6 + 1, rand() % 6 + 1));

        for (auto elem : tmp) {
            tmpBoard[elem.first]++;
            if (elem.second > -1)
                tmpBoard[elem.second]--;
        }
        for (int i : tmpBoard)
            if (i < 0)
                flag = 0;
        if (flag)
            return tmpBoard;
        first.update(tmp);
    }
    return tmpBoard;
}

void RandomLearningStrat::generateLearningData(int games, std::string filename) {
    std::vector<int> valueMe(24), valueEnemy(24), valueWalls(24), valueWalls2(24), valueEnemyWalls(24), valueEnemyWalls2(24);
    int valueFirst, valueLast, valueNumMe, valueNumEnemy, valueAreaMe, valueAreaEnemy, wins;
    bool firstFlag;
    RandomStrat first(1);
    RandomStrat second(0);
    Arbitrator arb(&first, &second, 100);
    std::ofstream out;
    out.open(filename, std::ios_base::out | std::ios_base::trunc);
    std::vector<int> tmp;
    if (out.is_open()) {
        for (int k = 0; k < games; k++) {
            std::cout << k << std::endl;

            tmp = genRandomPos(first, second);
            valueLast = valueNumMe = valueNumEnemy = valueAreaMe =
                    valueAreaEnemy = 0;
            firstFlag = 0;
            valueFirst = 24;

            for (int i = 0; i < 24; i++) {
                valueEnemyWalls[i] = valueWalls[i] = 0;
                if (tmp[i] < 0) {
                    valueAreaEnemy++;
                    valueNumEnemy -= tmp[i];
                    valueMe[i] = 0;
                    valueEnemy[i] = -tmp[i];
                    for (int j = 0; tmp[(i + j) % 24] < 0; j++)
                        valueEnemyWalls[i]++;
                    valueEnemyWalls2[i] = valueEnemyWalls[i] * valueEnemyWalls[i];
                } else if (tmp[i] > 0) {
                    if (!firstFlag) {
                        firstFlag = 1;
                        valueFirst = i;
                    }
                    if (i > valueLast)
                        valueLast = i;
                    valueAreaMe++;
                    valueNumMe += tmp[i];
                    valueEnemy[i] = 0;
                    valueMe[i] = tmp[i];
                    for (int j = 0; tmp[(i + j) % 24] > 0; j++)
                        valueWalls[i]++;
                    valueWalls2[i] = valueWalls[i] * valueWalls[i];
                } else {
                    valueMe[i] = valueEnemy[i] = 0;
                }
            }
            for (int i = 0; i < 24; i++) {
                out << valueMe[i] << ' ' << valueEnemy[i] << ' ' << valueWalls[i] << ' ' << valueWalls2[i]
                       << ' ' << valueEnemyWalls[i] << ' ' << valueEnemyWalls2[i] << ' ';
            }

            if (!valueNumMe)
                valueFirst = valueLast = 24;
            out << valueFirst << ' ' << valueLast << ' ' << valueNumMe << ' ' << valueNumEnemy << ' '
                << valueAreaMe << ' ' << valueAreaEnemy << ' ';


            arb.setBoard(tmp);
            wins = arb.firstWins();
            out << ((double)wins)/100 << std::endl;
            //out.flush();
        }
    }
    else
        std::cout << "Unable to open file\n";
}
