#ifndef NEURONSTRAT_H
#define NEURONSTRAT_H

#include <string>
#include "betteristrategy.h"
#include "movesgenerator.h"

typedef void (*ParamsCalculator)(const std::vector<int>&, std::vector<double>&);

class NeuronStrat : public BetterIStrategy
{
private:
    std::vector<int> board;
    bool isWhite, useIntercepts;
    MovesGenerator generator;

    size_t params, layers, layersize;
    std::vector<std::vector<std::vector<double>>> coeffs;
    std::vector<std::vector<double>> intercepts;
    ParamsCalculator calc;
    std::string filename;

    void writeCoeffs();
    double possibility(const std::vector<int>& board);
    static std::vector<int> genRandomPos(BetterIStrategy* first, BetterIStrategy* second);

    double sigmoid(double x);

protected:
    TMove moveFromPos(const std::vector<TMove> &possibleMoves);

public:
    NeuronStrat(bool isWHite, size_t layers, size_t params, size_t layersize,
                ParamsCalculator calc, std::string filename = "neuroncoeffs.txt");

    void setIntercepts(std::string filename);

    static void generateLearningData(BetterIStrategy* first, BetterIStrategy* second, size_t games,
                                     ParamsCalculator calc, size_t params, std::string filename = "neuronlearningdata.txt");

    TMove move(const std::pair<int, int> &dice);
    void update(const TMove &moves);
    void reset();
    void changeSide();
    void setBoard(const std::vector<int> &board);

    //void TDlearn(double lambda, size_t games);
};

#endif // NEURONSTRAT_H
