#include "tdlstrat.h"
#include "arbitrator.h"
#include "randomstrat.h"
#include <cmath>
#include <fstream>
#include <iostream>

#define sqr(a) a*a

TDLstrat::TDLstrat(bool isWhite) {
    this->isWhite = isWhite;
    board = std::vector<int>(24, 0);
    board[0] = 15;
    board[12] = -15;
    generator = MovesGenerator();
    reloadCoeffs();
}

void TDLstrat::reloadCoeffs() {
    coeffFirst = coeffLast = coeffNumMe = coeffNumEnemy = coeffAreaMe = coeffAreaEnemy = 0;
    coeffsMe = coeffsEnemy = coeffsWalls  = coeffsWalls2 =
            coeffsEnemyWalls = coeffsEnemyWalls2 = std::vector<double>(24, 0);
    std::ifstream input("tdlcoeffs.txt");
    if (input.is_open()) {
        for (int i = 0; i < 24; i++)
            input >> coeffsMe[i] >> coeffsEnemy[i] >> coeffsWalls[i] >> coeffsWalls2[i]
                    >> coeffsEnemyWalls[i] >> coeffsEnemyWalls2[i];
        input >> coeffFirst >> coeffLast >> coeffNumMe >> coeffNumEnemy >> coeffAreaMe
                >> coeffAreaEnemy;
    } else
        std::cout << "Unable to open coeffs file\nSetting all coeffs to 0\n";
    input.close();
}

void TDLstrat::writeCoeffs() {
    std::ofstream out("tdlcoeffs.txt");
    if (out.is_open()) {
        for (int i = 0; i < 24; i++)
            out << coeffsMe[i] << ' ' << coeffsEnemy[i] << ' ' << coeffsWalls[i] <<
                   ' ' << coeffsWalls2[i] << ' ' << coeffsEnemyWalls[i] << ' ' << coeffsEnemyWalls2[i] << ' ';
        out << coeffFirst << ' ' << coeffLast << ' ' << coeffNumMe << ' ' << coeffNumEnemy
            << ' ' << coeffAreaMe << ' ' << coeffAreaEnemy;
    } else
        std::cout << "Unable to open coeffs file\n";
    out.close();
}

void  TDLstrat::learn(double lambda, int games) {
    std::vector<double> gradMe, gradEnemy, gradWalls, gradWalls2, gradEnemyWalls, gradEnemyWalls2;
    double gradLast, gradFirst, gradNumMe, gradNumEnemy, gradAreaMe, gradAreaEnemy;
    std::vector<int> valueMe[2], valueEnemy[2], valueWalls[2], valueWalls2[2],
            valueEnemyWalls[2], valueEnemyWalls2[2];
    int valueFirst[2], valueLast[2], valueNumMe[2], valueNumEnemy[2], valueAreaMe[2], valueAreaEnemy[2];
    valueMe[0] = valueEnemy[0] = valueWalls[0] = valueWalls2[0] =
            valueEnemyWalls[0] = valueEnemyWalls2[0] =
            valueMe[1] = valueEnemy[1] = valueWalls[1] = valueWalls2[1] =
            valueEnemyWalls[1] = valueEnemyWalls2[1] = std::vector<int>(24);
    gradMe = gradEnemy = gradWalls = gradWalls2 = gradEnemyWalls = gradEnemyWalls2 = std::vector<double>(24);
    isWhite = 1;
    double poss, possPrev;
    long double module;
    std::vector<int> tmp(24);
    bool side, firstFlag, gameover, win, firstTurn;
    TDLstrat enemy(0);
    TMove tm;
    for (int i = 0; i < games; i++) {
        //std::cout << i << '\n';
        tmp = std::vector<int>(24, 0);
        tmp[0] = 15;
        tmp[12] = -15;
        reset();
        enemy.reset();
        gameover = side = 0;
        firstTurn = 1;
        for(;;) {
            module = 0;
            //gradient
            if (!gameover) {
                valueLast[side] = valueNumMe[side] = valueNumEnemy[side]
                        = valueAreaMe[side] = valueAreaEnemy[side] = 0;
                firstFlag = 0;
                valueFirst[side] = 24;
                for (int i = 0; i < 24; i++) {
                    valueEnemyWalls[side][i] = valueWalls[side][i] =
                            valueEnemyWalls2[side][i] = valueWalls2[side][i] = 0;
                    if (tmp[i] < 0) {
                        valueAreaEnemy[side]++;
                        valueNumEnemy[side] -= tmp[i];
                        valueMe[side][i] = 0;
                        valueEnemy[side][i] = -tmp[i];
                        for (int j = 0; tmp[(i + j) % 24] < 0; j++)
                            valueEnemyWalls[side][i]++;
                        valueEnemyWalls2[side][i] = valueEnemyWalls[side][i] * valueEnemyWalls[side][i];
                    } else if (tmp[i] > 0) {
                        if (!firstFlag) {
                            firstFlag = 1;
                            valueFirst[side] = i;
                        }
                        if (i > valueLast[side])
                            valueLast[side] = i;
                        valueAreaMe[side]++;
                        valueNumMe[side] += tmp[i];
                        valueEnemy[side][i] = 0;
                        valueMe[side][i] = tmp[i];
                        for (int j = 0; tmp[(i + j) % 24] > 0; j++)
                            valueWalls[side][i]++;
                        valueWalls2[side][i] = valueWalls[side][i] * valueWalls[side][i];
                    } else {
                        valueMe[side][i] = valueEnemy[side][i] = 0;
                    }
                    if (!valueNumMe[side])
                        valueFirst[side] = valueLast[side] = 24;

                }
            }

            poss = possibility(tmp);

            if (!firstTurn) {
                if (gameover) {
                    valueLast[!side] = valueNumMe[!side] = valueNumEnemy[!side]
                            = valueAreaMe[!side] = valueAreaEnemy[!side] = valueFirst[!side] = 0;
                    valueMe[!side] = valueEnemy[!side] = valueWalls[!side] = valueWalls2[!side] =
                            valueEnemyWalls[!side] = valueEnemyWalls2[!side] = std::vector<int>(24, 0);
                    if (win)
                        poss = 1;
                    else
                        poss = 0;
                    side = !side;
                }
                if (poss != possPrev) {

                    for (int i = 0; i < 24; i++) {
                        gradMe[i] = (valueMe[side][i] - valueMe[!side][i])
                                * (poss - possPrev);
                        gradEnemy[i] = (valueEnemy[side][i] - valueEnemy[!side][i])
                                * (poss - possPrev);
                        gradWalls[i] = (valueWalls[side][i] - valueWalls[!side][i])
                                * (poss - possPrev);
                        gradWalls2[i] = (valueWalls2[side][i] - valueWalls2[!side][i])
                                * (poss - possPrev);
                        gradEnemyWalls[i] = (valueEnemyWalls[side][i] - valueEnemyWalls[!side][i])
                                * (poss - possPrev);
                        gradEnemyWalls2[i] = (valueEnemyWalls2[side][i] - valueEnemyWalls2[!side][i])
                                * (poss - possPrev);
                        /*module += sqr(gradMe[i]) + sqr(gradEnemy[i]) + sqr(gradWalls[i]) + sqr(gradWalls2[i])
                                + sqr(gradEnemyWalls[i]) + sqr(gradEnemyWalls2[i]);*/
                    }
                    gradLast = (valueLast[side] - valueLast[!side])
                            * (poss - possPrev);
                    gradFirst = (valueFirst[side] - valueFirst[!side])
                            * (poss - possPrev);
                    gradNumMe = (valueNumMe[side] - valueNumMe[!side])
                            * (poss - possPrev);
                    gradNumEnemy = (valueNumEnemy[side] - valueNumEnemy[!side])
                            * (poss - possPrev);
                    gradAreaMe = (valueAreaMe[side] - valueAreaMe[!side])
                            * (poss - possPrev);
                    gradAreaEnemy = (valueAreaEnemy[side] - valueAreaEnemy[!side])
                            * (poss - possPrev);
                    /*module += sqr(gradLast) + sqr(gradFirst) + sqr(gradNumMe) + sqr(gradNumEnemy) +
                            sqr(gradAreaEnemy) + sqr(gradAreaMe);*/

                    /*module = std::sqrt(module);*/
                    module = 1;

                    if (module != 0) {
                        for (int i = 0; i < 24; i++) {
                            coeffsMe[i] -= lambda * gradMe[i]/module;
                            coeffsEnemy[i] -= lambda * gradEnemy[i]/module;
                            coeffsWalls[i] -= lambda * gradWalls[i]/module;
                            coeffsWalls2[i] -= lambda * gradWalls2[i]/module;
                            coeffsEnemyWalls[i] -= lambda * gradEnemyWalls[i]/module;
                            coeffsEnemyWalls2[i] -= lambda * gradEnemyWalls2[i]/module;
                        }
                        coeffLast -= lambda * gradLast/module;
                        coeffFirst -= lambda * gradFirst/module;
                        coeffNumMe -= lambda * gradNumMe/module;
                        coeffNumEnemy -= lambda * gradNumEnemy/module;
                        coeffAreaMe -= lambda * gradAreaMe/module;
                        coeffAreaEnemy -= lambda * gradAreaEnemy/module;

                        enemy.coeffsMe = coeffsMe;
                        enemy.coeffsEnemy = coeffsEnemy;
                        enemy.coeffsWalls = coeffsWalls;
                        enemy.coeffsWalls2 = coeffsWalls2;
                        enemy.coeffsEnemyWalls = coeffsEnemyWalls;
                        enemy.coeffsEnemyWalls2 = coeffsEnemyWalls2;
                        enemy.coeffLast = coeffLast;
                        enemy.coeffFirst = coeffFirst;
                        enemy.coeffNumMe = coeffNumMe;
                        enemy.coeffNumEnemy = coeffNumEnemy;
                        enemy.coeffAreaMe = coeffAreaMe;
                        enemy.coeffAreaEnemy = coeffAreaEnemy;
                    }
                }
            }
            possPrev = poss;

            if (gameover)
                break;

            //game cycle
            //white's turn
            gameover = 1;
            tm = move(std::make_pair(rand() % 6 + 1, rand() % 6 + 1));

            for (auto elem : tm) {
                tmp[elem.first]--;
                if (elem.second > -1)
                    tmp[elem.second]++;
            }
            for (int i : tmp)
                if (i > 0)
                    gameover = 0;
            if (gameover) {
                win = 1;
                continue;
            }
            enemy.update(tm);

            //black's turn
            gameover = 1;
            tm = enemy.move(std::make_pair(rand() % 6 + 1, rand() % 6 + 1));

            for (auto elem : tm) {
                tmp[elem.first]++;
                if (elem.second > -1)
                    tmp[elem.second]--;
            }
            for (int i : tmp)
                if (i < 0)
                    gameover = 0;
            if (gameover) {
                win = 0;
                continue;
            }
            update(tm);

            side = !side;
            firstTurn = 0;
        }
    }
    writeCoeffs();
}
