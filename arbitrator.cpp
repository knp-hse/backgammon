#include "arbitrator.h"
#include "stdlib.h"
#include <iostream>

Arbitrator::Arbitrator(BetterIStrategy *first, BetterIStrategy *second, int games) {
    this->first = first;
    this->second = second;
    this->games = games;
    board = std::vector<int>(24, 0);
    board[0] = 15;
    board[12] = -15;
    specialBoard = 0;
}

Arbitrator::Arbitrator(BetterIStrategy* first, BetterIStrategy* second, int games, std::vector<int> board) {
    this->first = first;
    this->second = second;
    this->games = games;
    this->board = board;
    specialBoard = 1;
}

bool Arbitrator::play() {
    TMove tmp;
    bool flag;
    std::vector<int> tmpBoard = board;
    if (specialBoard) {
    first->setBoard(board);
    second->setBoard(board);
    } else {
        first->reset();
        second->reset();
    }
    for(;;) {
        //white's turn
        flag = 1;
        tmp = first->move(std::make_pair(rand() % 6 + 1, rand() % 6 + 1));

        for (auto elem : tmp) {
                tmpBoard[elem.first]--;
            if (elem.second > -1)
                tmpBoard[elem.second]++;
        }
        for (int i : tmpBoard)
            if (i > 0)
                flag = 0;
        if (flag)
            return 1;
        second->update(tmp);

        //black's turn
        flag = 1;
        tmp = second->move(std::make_pair(rand() % 6 + 1, rand() % 6 + 1));

        for (auto elem : tmp) {
            tmpBoard[elem.first]++;
            if (elem.second > -1)
                tmpBoard[elem.second]--;
        }
        for (int i : tmpBoard)
            if (i < 0)
                flag = 0;
        if (flag)
            return 0;
        first->update(tmp);
    }
}

void Arbitrator::setBoard(std::vector<int> board) {
    this->board = board;
    specialBoard = 1;
}

int Arbitrator::firstWins() {
    if (specialBoard) {
        bool flag = 1;
        for (int i : board)
            if (i > 0)
                flag = 0;
        if (flag)
            return games;
        for (int i : board)
            if (i < 0)
                flag = 0;
        if (flag)
            return 0;
    }
    //srand(time(0));
    int ans = 0;
    bool side = 1;
    for (int i = 0; i < games; i++) {
        //std::cout << "playing game: " << i << std::endl;
        if (side == play())
            ans++;
        if (!specialBoard) {
            first->changeSide();
            second->changeSide();
            std::swap(first, second);
            side = !side;
        }
    }
    return ans;
}
